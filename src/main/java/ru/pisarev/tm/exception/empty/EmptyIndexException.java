package ru.pisarev.tm.exception.empty;

import ru.pisarev.tm.exception.AbstractException;

public class EmptyIndexException extends AbstractException {

    public EmptyIndexException() {
        super("Error. Index is empty");
    }

}
