package ru.pisarev.tm.exception.system;

import ru.pisarev.tm.constant.TerminalConst;
import ru.pisarev.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException() {
        super("Incorrect command. Use " + TerminalConst.CMD_HELP + " for display list of terminal commands.");
    }

}
